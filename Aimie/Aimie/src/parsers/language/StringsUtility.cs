﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Aimie
{
	public class StringsUtility
	{
		private StringsUtility ()
		{
		}

		public static Dictionary<string,Dictionary<string,string>> ReadStrings(string file)
		{
			Dictionary<string,Dictionary<string,string>> strings = new Dictionary<string,Dictionary<string,string>> ();
			foreach (string lang in Core.LANGUAGES)
			{
				Dictionary<string,string> languageStrings = new Dictionary<string,string>();
				string[] lines = null;
				try {
					lines = System.IO.File.ReadAllLines(@file);
				} catch (FileNotFoundException ex) {
					return null;
				}
				foreach (string line in lines)
				{
					string[] splitLine = line.Split (':');
					if (lang.Equals (splitLine [0].Trim ()))
					{
						string[] phrase = splitLine [1].Split (new char[] { '=' }, 2);
						languageStrings.Add (phrase [0].Trim (), phrase [1].Trim ());
					}
				}
				strings.Add (lang, languageStrings);
			}

			return strings;
		}

		public static void WriteStrings(Dictionary<string,Dictionary<string,Dictionary<string,string>>> strings)
		{
			foreach (string file in strings.Keys) {
				List<string> lines = new List<string> ();
				Dictionary<string,Dictionary<string,string>> fileStrings = strings [file];
				foreach (string lang in Core.LANGUAGES) {
					Dictionary<string,string> langStrings = fileStrings [lang];
					foreach (string key in langStrings.Keys) {
						string value = langStrings [key];
						lines.Add (lang + ":" + key + " = " + value);
					}
				}

				System.IO.File.WriteAllLines (@file, lines.ToArray ());
			}
		}
	}
}

