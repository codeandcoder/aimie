﻿using System;
using System.Collections.Generic;

namespace Aimie
{
	public class GrammarFile
	{
		private string id;
		public string ID
		{
			get { return id; }
			set { id = value; }
		}

		private Dictionary<string,List<Sentence>> sentences;
		public  Dictionary<string,List<Sentence>> Sentences
		{
			get { return sentences; }
			set { sentences = value; }
		}
		public void AddSentence(Sentence sent)
		{
			sentences[sent.Language].Add (sent);
		}

		public GrammarFile (string id, Dictionary<string,List<Sentence>> sentences)
		{
			this.id = id;
			this.sentences = sentences;
		}

		public List<string> GetWholeGrammar(string lang)
		{
			List<string> wholeGrammar = new List<string> ();
			List<Sentence> sents = sentences [lang];

			foreach (Sentence sentence in sents)
			{
				wholeGrammar.AddRange (sentence.GetAllPosibilities());
			}

			return wholeGrammar;
		}

		public void AddSentenceInLanguages (Dictionary<string,Sentence> sents)
		{
			if (sents.Keys.Count < sentences.Keys.Count) {
				throw new Exception ("Lacks some languages");
			} else if (sents.Keys.Count > sentences.Keys.Count) {
				throw new Exception ("Too many languages! Maybe some are not supported yet");
			}

			foreach (string key in sents.Keys)
			{
				sentences [key].Add (sents[key]);
			}
		}

		public Sentence GetSentenceWithText(string text, string lang)
		{
			List<Sentence> sents = sentences [lang];

			foreach (Sentence sentence in sents)
			{
				List<string> posibs = sentence.GetAllPosibilities();
				if ( posibs.Contains(text) )
				{
					return sentence;
				}
			}

			return null;
		}

		public Sentence GetSentenceByID(string id, string lang)
		{
			List<Sentence> sents = sentences [lang];

			foreach (Sentence sentence in sents)
			{
				if ( sentence.ID == id )
				{
					return sentence;
				}
			}

			return null;
		}

		public void RemoveSentence(string id)
		{
			foreach (string lang in Core.LANGUAGES) {
				List<Sentence> sents = sentences [lang];
				Sentence sentenceToRemove = null;

				foreach (Sentence sentence in sents) {
					if (sentence.ID == id) {
						sentenceToRemove = sentence;
						break;
					}
				}

				if (sentenceToRemove != null)
					sentences [lang].Remove (sentenceToRemove);
			}
		}

		public List<string> FormatFile()
		{
			List<string> result = new List<string> ();
			foreach (string key in sentences.Keys)
			{
				List<string> sents = new List<string> ();
				List<string> opts = new List<string> ();
				foreach (Sentence s in sentences[key])
				{
					sents.Add (s.ToString ());
					foreach (Option o in s.Options)
					{
						foreach (Option subOpt in o.GetAllSubOpts()) {
							string subOptString = subOpt.ToString ();
							if ( !opts.Contains(subOptString) )
							{
								opts.Add (subOptString);
							}
						}
						string opt = o.ToString ();
						if ( !opts.Contains(opt) )
						{
							opts.Add (opt);
						}
					}
				}
				result.AddRange (opts);
				result.Add ("");
				result.AddRange (sents);
				result.Add ("");
				result.Add ("");
			}

			return result;
		}
	}
}

