﻿using System;
using System.Collections.Generic;

namespace Aimie
{
	public class Sentence
	{
		private string sentence;

		private string id;
		public string ID
		{
			get { return id; }
			set { id = value; }
		}

		private string language;
		public string Language
		{
			get { return language; }
			set { language = value; }
		}

		private List<Option> options;
		public List<Option> Options
		{
			get { return options; }
			set { options = value; } 
		}
		public void AddOptions(List<Option> opts)
		{
			options.AddRange (opts);
		}
		public void RemoveOptionWithID(string id)
		{
			Option toRemove = null;
			foreach (Option o in options) {
				if (o.ID == id) {
					toRemove = o;
				}
			}
			if (toRemove != null) {
				options.Remove (toRemove);
			}
		}

		private List<string> parameters;

		public Sentence (string id, string baseSentence, List<Option> opts, string lang, List<string> parameters)
		{
			this.id = id;
			this.sentence = baseSentence;
			this.options = opts == null ? new List<Option>() : opts;
			this.language = lang;
			this.parameters = parameters;
		}

		public List<string> GetParameters(string text)
		{
			List<string> _params = new List<string> ();
			foreach ( Option o in options ) {
				if ( parameters.Contains(o.ID) ) {
					foreach (string val in o.GetAllPosibilities()) {
						if ( text.Contains(val) ) {
							_params.Add (val);
						}
					}
				}
			}
			return _params;
		}

		public List<string> GetAllPosibilities()
		{
			List<string> posibilities = new List<string> ();

			if (options.Count > 0) { 
				List<List<string>> permutations = CalculatePermutations ();

				foreach (List<string> perm in permutations)
				{
					string currentPos = sentence;
					for (int i = 0; i < perm.Count; i++)
					{
						currentPos = currentPos.Replace ("<" + options [i].ID + ">", perm [i]).Replace("  ", " ");
						currentPos = currentPos.Replace ("<?" + options [i].ID + ">", perm [i]).Replace("  ", " ");
					}
					if (!posibilities.Contains (currentPos)) {
						posibilities.Add (currentPos);
					}
				}
			} else {
				posibilities.Add (sentence);
			}

			return posibilities;
		}

		private List<List<string>> CalculatePermutations ()
		{
			List<List<string>> permutations = new List<List<string>> ();
			List<List<string>> optionPosibilities = new List<List<string>> ();
			List<string> currentPermutation = new List<string> ();
			foreach (Option o in options) {
				List<string> poss = o.GetAllPosibilities ();
				optionPosibilities.Add (poss);
				currentPermutation.Add (poss [0]);
			}
			Permutate(0,currentPermutation,permutations, optionPosibilities);
			return permutations;
		}

		private void Permutate(int pos, List<string> currentPermutation,
			List<List<string>> perms, List<List<string>> posibilities)
		{
			foreach (string val in posibilities[pos]) {
				currentPermutation [pos] = val;
				if (pos == options.Count - 1) {
					perms.Add (new List<string>(currentPermutation));
				} else {
					Permutate(pos+1,currentPermutation,perms,posibilities);
				}
			}
		}

		public override string ToString ()
		{
			return string.Format ("{0}:sen:{1} = {2}",language, ID, sentence);
		}
			
	}

}

