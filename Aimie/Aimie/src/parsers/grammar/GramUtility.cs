﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

namespace Aimie
{
	public class GramUtility
	{
		private GramUtility ()
		{
		}

		public static GrammarFile ReadGrammarFile(string file)
		{
			Dictionary<string,List<Sentence>> langSentences = new Dictionary<string,List<Sentence>> ();
			foreach (string lang in Core.LANGUAGES)
			{
				Dictionary<String,Option> fileOptions = ParseFileOptions (file, lang);
				BuildOptionTree (fileOptions);
				List<Sentence> sentences = ParseFileSentences (file, fileOptions, lang);
				if (fileOptions != null && sentences != null) {
					langSentences.Add (lang, sentences);
				}
			}

			string[] fileParts = file.Split ('\\');
			return new GrammarFile (fileParts [fileParts.Length - 1], langSentences);
		}

		private static Dictionary<String,Option> ParseFileOptions(string file, string lang)
		{
			Dictionary<String,Option> options = new Dictionary<String,Option> ();

			string[] lines = null;
			try {
				lines = System.IO.File.ReadAllLines(@file);
			} catch (FileNotFoundException ex) {
				return null;
			}
			foreach (string line in lines)
			{
				// if line is not a comment
				if ( line.Length >= 2 && !"//".Equals (line.Trim ().Substring (0, 2)))
				{
					// splited line
					string[] splitLine = line.Split (new char[] { ':' }, 3);
					// is same language
					if (lang.Equals (splitLine [0].Trim ())) {
						// is an option
						if ("opt".Equals (splitLine [1].Trim ())) {
							string[] parts = splitLine [2].Split (new char[] { '=' }, 2);
							string id = parts [0].Trim ();
							string[] values = parts [1].Split ('|');
							List<string> formattedValues = FilterValues (values);
							List<string> optionIDs = FindOptionIDs (parts [1]);
							Option opt = new Option (id, formattedValues, null, lang);
							if (!options.ContainsKey (id)) {
								options.Add (id, opt);
							}
						}
					}
				}
			}

			return options;
		}

		private static void BuildOptionTree(Dictionary<String,Option> options)
		{
			foreach (string key in options.Keys) {
				Option o = options [key];
				string optionPhrase = o.ToString ().Split (new char[] { ':' }, 3) [2].Split (new char[] { '=' }, 2) [1];
				List<string> optionIDs = FindOptionIDs (optionPhrase);
				List<Option> optOptions = new List<Option> ();
				foreach (string optID in optionIDs) {
					optOptions.Add (options [optID]);
				}
				o.Options = optOptions;
			}
		}

		private static List<string> FilterValues(string[] vals)
		{
			List<string> values = new List<string> ();
			foreach (string val in vals)
			{
				values.Add (val.Trim ().Replace ("!!", ""));
			}
			return values;
		}

		private static List<Sentence> ParseFileSentences(string file, Dictionary<String,Option> opts, string lang)
		{
			List<Sentence> sentences = new List<Sentence> ();

			string[] lines = null;
			try {
				lines = System.IO.File.ReadAllLines(@file);
			} catch (FileNotFoundException ex) {
				return null;
			}
			foreach (string line in lines)
			{
				// if line is not a comment
				if ( line.Length >= 2 && !"//".Equals (line.Trim ().Substring (0, 2)))
				{
					// splited line
					string[] splitLine = line.Split (new char[] { ':' }, 3);
					// is same language
					if (lang.Equals (splitLine [0].Trim ())) {
						// is a sentence
						if ("sen".Equals (splitLine [1].Trim ())) {
							string[] parts = splitLine [2].Split (new char[] { '=' }, 2);
							string id = parts [0].Trim ();
							List<string> optionIDs = FindOptionIDs (parts [1].Trim ());
							List<Option> options = new List<Option> ();
							List<string> parameters = new List<string> ();
							for (int i = 0; i < optionIDs.Count; i++) {
								String optID = optionIDs [i];
								if (optID.Substring (0, 1) == "?") {
									optID = optID.Substring (1);
									parameters.Add (optID);
								}
								Option opt = opts [optID];
								options.Add (opt);
							}
							Sentence sentence = new Sentence (id, parts [1].Trim (), options, lang, parameters);
							sentences.Add (sentence);
						}
					}
				}
			}

			return sentences;
		}

		private static List<string> FindOptionIDs(string sentence)
		{
			List<string> optionIDs = new List<string> ();
			string line = sentence.Clone() as string;
			int index = line.IndexOf ('<');
			while (index != -1)
			{
				int length = line.IndexOf ('>') - index - 1;
				string optionID = line.Substring (index+1, length);
				optionIDs.Add (optionID);

				line = line.Substring (line.IndexOf ('>')+1);

				index = line.IndexOf ('<');
			}

			return optionIDs;
		}

		public static void SaveGrammarFile(string folder, GrammarFile grammar)
		{
			List<string> formattedGrammar = grammar.FormatFile ();
			string path = folder.Substring (folder.Length - 1, 1) == "\\" ? folder : folder + "\\";
			path += grammar.ID;
			System.IO.File.WriteAllLines(@path, formattedGrammar.ToArray());
		}
	}
}

