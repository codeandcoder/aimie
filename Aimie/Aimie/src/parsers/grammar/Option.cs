﻿using System;
using System.Collections.Generic;

namespace Aimie
{
	public class Option
	{
		private string id;
		public string ID
		{
			get { return id; }
			set { id = value; }
		}

		private string language;
		public string Language
		{
			get { return language; }
			set { language = value; }
		}

		private List<string> values;
		public  List<string> Values
		{
			get { return values; }
			set { values = value; }
		}

		private List<Option> options;
		public List<Option> Options
		{
			get { return options; }
			set { options = value; }
		}

		public Option (string id, List<string> values, List<Option> opts, string lang)
		{
			this.id = id;
			this.values = values;
			this.language = lang;
			this.options = opts == null ? new List<Option>() : opts;
		}

		public List<string> GetAllPosibilities()
		{
			List<string> posibilities = new List<string> ();

			foreach (string _value in values) {
				List<Option> options = new List<Option> ();
				for (int i = 0; i < this.options.Count; i++) {
					if (_value.Contains (this.options [i].ID) && !options.Contains(this.options[i])) {
						options.Add (this.options [i]);
					}
				}

				if (options.Count > 0) { 
					List<List<string>> permutations = CalculatePermutations (options);

					foreach (List<string> perm in permutations) {
						string currentPos = _value;
						for (int i = 0; i < perm.Count; i++) {
							currentPos = currentPos.Replace ("<" + options [i].ID + ">", perm [i]).Replace ("  ", " ");
							currentPos = currentPos.Replace ("<?" + options [i].ID + ">", perm [i]).Replace ("  ", " ");
						}
						if (!posibilities.Contains (currentPos)) {
							posibilities.Add (currentPos);
						}
					}

				} else {
					posibilities.Add (_value);
				}
			}

			if (posibilities.Count == 0) {
				posibilities.Add ("");
			}

			return posibilities;
		}

		private List<List<string>> CalculatePermutations (List<Option> options)
		{
			List<List<string>> permutations = new List<List<string>> ();
			List<List<string>> optionPosibilities = new List<List<string>> ();
			List<string> currentPermutation = new List<string> ();
			foreach (Option o in options) {
				List<string> poss = o.GetAllPosibilities ();
				optionPosibilities.Add (poss);
				currentPermutation.Add (poss [0]);
			}
			Permutate(0,currentPermutation,permutations,optionPosibilities);
			return permutations;
		}

		private void Permutate(int pos, List<string> currentPermutation,
			List<List<string>> perms, List<List<string>> posibilities)
		{
			foreach (string val in posibilities[pos]) {
				currentPermutation [pos] = val;
				if (pos == posibilities.Count - 1) {
					perms.Add (new List<string>(currentPermutation));
				} else {
					Permutate(pos+1,currentPermutation,perms, posibilities);
				}
			}
		}

		public List<Option> GetAllSubOpts()
		{
			List<Option> subOpts = new List<Option> ();
			subOpts.AddRange (options);
			foreach (Option o in options)
			{
				subOpts.AddRange (o.GetAllSubOpts());
			}
			return subOpts;
		}

		private string GetVals()
		{
			string vals = "";

			bool isFirst = true;
			foreach (string value in values)
			{
				if (isFirst) {
					isFirst = false;
				} else {
					vals += " | ";
				}
				vals += value.Trim() == "" ? "!!" : value;
			}

			return vals;
		}

		public override string ToString ()
		{
			string vals = GetVals ();
			return string.Format ("{0}:opt:{1} = {2}", language, ID, vals);
		}
	}
}

