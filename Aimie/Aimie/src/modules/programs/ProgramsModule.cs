﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Aimie
{
	/*
	 * Program running related functionality
	 */
	public class ProgramsModule : Module
	{
		private Dictionary<string,Process> programs;

		public override void Execute(string sentenceID, List<string> args, int caseCode)
		{
			if (sentenceID.Contains ("run-somefile")) {
				string path = Configuration.GetFile (args [0]);
				RunProgram (path, args [0]);
			} else if (sentenceID.Contains ("ask-running")) {
				if (programs.Count > 0) {
					if (programs.Count == 1) {
						string progs = core.Language.GetString ("one");
						core.Speaker.Speak ("running-some", progs);
					} else {
						string progs = ReplaceArgs(core.Language.GetString ("many"), new string[] {programs.Count + ""});
						core.Speaker.Speak ("running-some", progs);
					}
				} else {
					core.Speaker.Speak (core.Language.GetString ("no-running"));
				}
			} else if (sentenceID.Contains ("stop-running")) {
				StopProgram (args [0]);
			}
		}

		public override GrammarFile RetrieveGrammarFile()
		{
			GrammarFile gram = GramUtility.ReadGrammarFile (RetrieveGrammarFileName ());
			// We generate some recognition phrases in runtime
			// in order to add configuration files to the grammar file and
			// so that, they can be recognized
			if (Configuration.Files.Count > 0) {
				List<string> files = new List<string> ();
				List<string> parameters = new List<string> ();
				parameters.Add ("program-options");
				foreach (string key in Configuration.Files.Keys) {
					files.Add (key);
				}
				foreach (string lang in Core.LANGUAGES) {
					Option aimieOpt = gram.GetSentenceByID ("ask-running", lang).Options [0];
					List<Option> options = new List<Option> ();
					options.Add (aimieOpt);
					Option opt = new Option ("program-options", files, null, lang);
					options.Add (opt);
					Sentence sent = gram.GetSentenceByID ("run-somefile", lang);
					if (sent == null) {
						sent = new Sentence ("run-somefile", "<aimie> " + core.Language.GetString ("run") + " <?program-options>",
							options, lang, parameters);
						gram.AddSentence (sent);
					} else {
						sent.RemoveOptionWithID ("aimie");
						sent.RemoveOptionWithID ("program-options");
						sent.AddOptions (options);
					}
					Sentence sent2 = gram.GetSentenceByID ("stop-running", lang);
					if (sent2 == null) {
						sent2 = new Sentence ("stop-running", "<aimie> " + core.Language.GetString ("stop-running-now") + " <?program-options>",
							options, lang, parameters);
						gram.AddSentence (sent2);
					}
				}
			} else {
				gram.RemoveSentence ("run-somefile");
				gram.RemoveSentence ("stop-running");
			}
			return gram;
		}

		public override Dictionary<string,Dictionary<string,string>> RetrieveStringsFile ()
		{
			return StringsUtility.ReadStrings(RetrieveStringsFileName());
		}

		public override string RetrieveGrammarFileName()
		{
			return "resources\\grammars\\programs.gram";
		}

		public override string RetrieveStringsFileName ()
		{
			return "resources\\language\\programs.strings";
		}

		private void RunProgram(string file, string pronunciation)
		{
			if (programs == null) {
				programs = new Dictionary<string, Process> ();
			}
			programs.Add(pronunciation, Process.Start(file));
			core.Speaker.Speak ("executing", pronunciation);
		}

		private void StopProgram(string pronunciation)
		{
			if (programs [pronunciation] != null) {
				try {
					programs [pronunciation].Kill ();
					programs.Remove (pronunciation);
					core.Speaker.Speak ("stop-executing", pronunciation);
				} catch (Exception ex) {
					core.Speaker.Speak ("stop-executing-fail", pronunciation);
				}
			} else {
				core.Speaker.Speak ("stop-executing-fail", pronunciation);
			}
		}

		private String ReplaceArgs(string sentence, string[] args)
		{
			string replacedSentence = sentence;
			foreach (string arg in args)
			{
				Regex regex = new Regex(Regex.Escape("?"));
				replacedSentence = regex.Replace(replacedSentence, arg, 1); 
			}

			return replacedSentence;
		}
	}
}

