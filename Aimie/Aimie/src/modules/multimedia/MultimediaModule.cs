using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;

namespace Aimie
{
	/*
	 * Multimedia related functionality
	 */
	public class MultimediaModule : Module
	{
		private Process player;

		public override void Execute(string sentenceID, List<string> args, int caseCode)
		{
			if (args.Count > 0) {
				string key = args [0];
				PlayMultimedia (Configuration.GetFile(key), key);
			} else if (sentenceID.Contains("play")) {
				string[] idParts = sentenceID.Split ('-');
				string multimedia = idParts [idParts.Length - 1];
				string file = null;
				if (sentenceID.Contains ("music")) {
					string folder = Configuration.GetDir (core.Language.GetString ("music-folder"));
					if (folder == null) {
						core.Speaker.Speak ("music-folder-problem");
						return;
					} else {
						file = FindFile (multimedia, folder);
					}
				} else if (sentenceID.Contains("film")) {
					string folder = Configuration.GetDir (core.Language.GetString ("films-folder"));
					if (folder == null) {
						core.Speaker.Speak ("films-folder-problem");
						return;
					} else {
						file = FindFile (multimedia, folder);
					}
				}
				if (file != null) {
					PlayMultimedia (file, GetFileName(file));
				} else {
					core.Speaker.Speak ("multimedia-not-found", multimedia);
				}
			} else if (sentenceID.Contains("stop")) {
				StopMultimedia ();
			}
		}

		public override GrammarFile RetrieveGrammarFile()
		{
			GrammarFile gram = GramUtility.ReadGrammarFile (RetrieveGrammarFileName ());
			// We generate some recognition phrases in runtime
			// in order to add configuration files to the grammar file and
			// so that, they can be recognized
			if (Configuration.Files.Count > 0) {
				List<string> files = new List<string> ();
				List<string> parameters = new List<string> ();
				parameters.Add ("files-options");
				foreach (string key in Configuration.Files.Keys) {
					files.Add (key);
				}
				foreach (string lang in Core.LANGUAGES) {
					Option aimieOpt = gram.GetSentenceByID ("stop", lang).Options [0];
					List<Option> options = new List<Option> ();
					options.Add (aimieOpt);
					Option opt = new Option ("files-options", files, null, lang);
					options.Add (opt);
					Sentence sent = gram.GetSentenceByID ("play-somefile", lang);
					if (sent == null) {
						sent = new Sentence ("play-somefile", "<aimie> " + core.Language.GetString ("play") + " <?files-options>",
							options, lang, parameters);
						gram.AddSentence (sent);
					} else {
						sent.RemoveOptionWithID ("aimie");
						sent.RemoveOptionWithID ("files-options");
						sent.AddOptions (options);
					}
				}
			} else {
				gram.RemoveSentence ("play-somefile");
			}
			return gram;
		}

		public override Dictionary<string,Dictionary<string,string>> RetrieveStringsFile ()
		{
			return StringsUtility.ReadStrings(RetrieveStringsFileName());
		}

		public override string RetrieveGrammarFileName()
		{
			return "resources\\grammars\\multimedia.gram";
		}

		public override string RetrieveStringsFileName ()
		{
			return "resources\\language\\multimedia.strings";
		}

		private string FindFile(string name, string folder)
		{
			if (name == "somemusic" || name == "somefilm") {
				string[] posibilities = GetAllFilesInDirectory (folder, 3);
				return RandomUtility.ChooseRandomly (posibilities);
			} else {
				foreach (string file in Directory.GetFiles(folder)) {
					string fileName = GetFileName (file);
					if (fileName.Contains (name) && IsMultimedia(name)) {
						return file;
					}
				}
				foreach (string dir in Directory.GetDirectories(folder) ) {
					string found = FindFile (name, dir);
					if (found != null) {
						return found;
					}
				}
			}

			return null;
		}

		private string[] GetAllFilesInDirectory(string folder, int depth)
		{
			List<string> result = new List<string> ();
			foreach (string file in Directory.GetFiles(folder)) {
				if (IsMultimedia (file)) {
					result.Add (file);
				}
			}
			if (depth > 0) {
				foreach (string dir in Directory.GetDirectories(folder)) {
					result.AddRange (GetAllFilesInDirectory (dir, depth - 1));
				}
			}
			return result.ToArray();
		}

		private string GetFileName(string path)
		{
			string[] pathSplitted = path.Split ('\\');
			return pathSplitted [pathSplitted.Length - 1];
		}

		private bool IsMultimedia(string name)
		{
			string[] nameParts = name.Split('.');
			String extension = nameParts.Length > 1 ? nameParts[nameParts.Length-1] : null;
			return extension == "mp3" || extension == "mp4" 
				|| extension == "wav" || extension == "ogg" 
				|| extension == "avi" || extension == "mpg" 
				|| extension == "mpeg";
		}

		private void PlayMultimedia(string file, string pronunciation)
		{
			player = Process.Start(file);
			core.Speaker.Speak ("playing", pronunciation);
		}

		private void StopMultimedia()
		{
			if (player != null) {
				try {
					player.Kill ();
					player = null;
					core.Speaker.Speak ("stop-playing");
				} catch (Exception ex) {
					core.Speaker.Speak ("stop-music-fail");
				}
			} else {
				core.Speaker.Speak ("stop-music-fail");
			}
		}
	}
}

