using System;
using System.Collections.Generic;

namespace Aimie
{
	public abstract class Module
	{
		protected Core core;

		public Module ()
		{
		}

		public virtual void Init(Core core)
		{
			this.core = core;
		}

		public abstract void Execute(string sentenceID, List<string> args, int caseCode);
		public abstract GrammarFile RetrieveGrammarFile();
		public abstract string RetrieveGrammarFileName();
		public abstract Dictionary<string,Dictionary<string,string>> RetrieveStringsFile();
		public abstract string RetrieveStringsFileName();
	}
}

