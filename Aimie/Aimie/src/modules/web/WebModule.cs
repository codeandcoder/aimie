﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Aimie
{
	public class WebModule : Module
	{
		private Process browser;

		public override void Execute(string sentenceID, List<string> args, int caseCode)
		{
			if (sentenceID.Contains ("open-web")) {
				string key = args [0];
				string url = Configuration.GetWeb (key);
				OpenWeb (url, key);
			} else if (sentenceID.Contains ("close-web")) {
				CloseWeb ();
			}
		}

		public override GrammarFile RetrieveGrammarFile()
		{
			GrammarFile gram = GramUtility.ReadGrammarFile (RetrieveGrammarFileName ());
			if (Configuration.Webs.Count > 0) {
				List<string> webs = new List<string> ();
				List<string> parameters = new List<string> ();
				parameters.Add ("webs-options");
				foreach (string key in Configuration.Webs.Keys) {
					webs.Add (key);
				}
				foreach (string lang in Core.LANGUAGES) {
					Option aimieOpt = gram.GetSentenceByID ("close-web", lang).Options [0];
					List<Option> options = new List<Option> ();
					options.Add (aimieOpt);
					Option opt = new Option ("webs-options", webs, null, lang);
					options.Add (opt);
					Sentence sent = gram.GetSentenceByID ("open-web", lang);
					if (sent == null) {
						string webStart = core.Language.GetString ("open-web-start");
						string webEnd = core.Language.GetString ("open-web-end") == null ? "" : core.Language.GetString ("open-web-end");
						sent = new Sentence ("open-web", "<aimie> " + webStart + " <?webs-options> " + webEnd,
							options, lang, parameters);
						gram.AddSentence (sent);
					} else {
						sent.RemoveOptionWithID ("aimie");
						sent.RemoveOptionWithID ("webs-options");
						sent.AddOptions (options);
					}
				}
			} else {
				gram.RemoveSentence ("open-web");
			}
			return gram;
		}
		public override string RetrieveGrammarFileName()
		{
			return "resources\\grammars\\web.gram";
		}
		public override Dictionary<string,Dictionary<string,string>> RetrieveStringsFile()
		{
			return StringsUtility.ReadStrings(RetrieveStringsFileName());
		}
		public override string RetrieveStringsFileName()
		{
			return "resources\\language\\web.strings";
		}

		private void OpenWeb(string url, string pronunciation) {
			try {
				browser = Process.Start(url);
				core.Speaker.Speak ("opening-web", pronunciation);
			} catch (System.ComponentModel.Win32Exception ex) {
				core.Speaker.Speak ("opening-web-fail", pronunciation);
			}
		}

		private void CloseWeb()
		{
			if (browser != null) {
				browser.Kill ();
				browser = null;
				core.Speaker.Speak ("closing-web");
			} else {
				core.Speaker.Speak ("closing-web-fail");
			}
		}
	}
}

