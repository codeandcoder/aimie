﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Aimie
{
	/*
	 * Basic Aimie functionality
	 */
	public class BasicModule : Module
	{
		private static readonly int SHUT_DOWN_CASE = 1;
		private int shutDownSeconds = 0;
		private int caseTries = 0;

		public override void Execute(string sentenceID, List<string> args, int caseCode)
		{
			if (caseCode == SHUT_DOWN_CASE) {
				if (sentenceID.Contains ("yes")) {
					if (shutDownSeconds == 0) {
						core.Speaker.Speak ("shutting-down");
						ShutDown (shutDownSeconds);
					} else if (shutDownSeconds <= 60) {
						ShutDown (shutDownSeconds);
						core.Speaker.Speak ("shutting-down-programmed", shutDownSeconds + " seconds");
					} else {
						ShutDown (shutDownSeconds);
						core.Speaker.Speak ("shutting-down-programmed", (shutDownSeconds / 60) + " minutes");
					}
				} else if (sentenceID.Contains ("no")) {
					core.Speaker.Speak ("shutting-aborted");
				} else if (caseTries <= 3) {
					core.SetResponseQueue (this, SHUT_DOWN_CASE);
					caseTries++;
				}
			} else if (sentenceID.Contains ("changelanguage")) {
				if (sentenceID.Contains ("spanish")) {
					core.ActiveLanguage = "es";
					core.Speaker.Speak ("lang-spanish");
				} else if (sentenceID.Contains ("english")) {
					core.ActiveLanguage = "en";
					core.Speaker.Speak ("lang-english");
				}
			} else if (sentenceID.Contains ("silence")) {
				if (sentenceID.Contains ("on")) {
					core.SilenceMode = true;
					core.Speaker.Speak ("deactivated");
				} else if (sentenceID.Contains ("off")) {
					core.SilenceMode = false;
					core.Speaker.Speak ("activated");
				}
			} else if (sentenceID.Equals ("shut-down")) {
				shutDownSeconds = 0;
				if (args.Count > 0) {
					shutDownSeconds = ExtractSeconds (args [0]);
				}
				core.SetResponseQueue (this, SHUT_DOWN_CASE);
				caseTries = 0;
				core.Speaker.Speak ("sure-shut-down");
			} else if (sentenceID.Contains ("open-conf")) {
				OpenConf ();
			} else if (sentenceID.Contains ("abort-shut-down")) {
				AbortShutDown ();
				core.Speaker.Speak ("aborted-shut-down");
			} else if (sentenceID.Contains ("reload-conf")) {
				ReloadConf ();
				core.Speaker.Speak ("conf-reloaded");
			} else if (sentenceID.Contains ("credits")) {
				core.Speaker.Speak ("say-credits");
				OpenBlog ();
			}
		}

		public override GrammarFile RetrieveGrammarFile()
		{
			return GramUtility.ReadGrammarFile(RetrieveGrammarFileName());
		}

		public override Dictionary<string,Dictionary<string,string>> RetrieveStringsFile ()
		{
			return StringsUtility.ReadStrings(RetrieveStringsFileName());
		}

		public override string RetrieveGrammarFileName()
		{
			return "resources\\grammars\\base.gram";
		}

		public override string RetrieveStringsFileName ()
		{
			return "resources\\language\\base.strings";
		}

		private void ShutDown(int seconds)
		{
			Process.Start("shutdown","/s /t " + seconds);
		}

		private void AbortShutDown()
		{
			Process.Start("shutdown","/a");
		}

		private void OpenBlog()
		{
			Process.Start("http://rasanti.wordpress.com/");
		}

		private void ReloadConf()
		{
			Configuration.Init ("config\\aimie.conf");
		}

		private void OpenConf()
		{
			var processInfo = new ProcessStartInfo("java.exe", "-jar Aimie-ConfUtility.jar")
			{
				CreateNoWindow = true,
				UseShellExecute = false
			};
			try {
				Process.Start (processInfo);
				core.Speaker.Speak ("opening-conf");
			} catch (Exception ex) {
				try {
					core.Speaker.Speak("conf-error");
					Process.Start(".");
				} catch (Exception ex2) {}
			}
		}

		private int ExtractSeconds(string text)
		{
			string[] parts = text.Split (' ');
			int number = 0;
			try {
				number = Int32.Parse(parts[1]);
			} catch (Exception ex) {
				number = 1;
			}
			int seconds = 0;

			if (parts [2].Contains (core.Language.GetString ("seconds"))
				|| parts [2].Contains (core.Language.GetString("second"))) {
				seconds = number;
			} else if (parts [2].Contains (core.Language.GetString ("minutes"))
				|| parts [2].Contains (core.Language.GetString("minute"))) {
				seconds = number * 60;
			} else if (parts [2].Contains (core.Language.GetString ("hours"))
				|| parts [2].Contains (core.Language.GetString("hour"))) {
				seconds = number * 3600;
			}

			return seconds;
		}
	}
}

