﻿using System;

namespace Aimie
{
	public class RandomUtility
	{
		private RandomUtility ()
		{
		}

		public static int GetRandomNumber(int n1, int n2)
		{
			Random random = new Random ();
			return random.Next (n1, n2);
		}

		public static string ChooseRandomly(string[] options)
		{
			int randomNumber = GetRandomNumber (0, options.Length);
			return options [randomNumber];
		}
	}
}

