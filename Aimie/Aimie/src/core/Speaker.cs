using System;
using Microsoft.Speech.Synthesis;
using System.Text.RegularExpressions;

namespace Aimie
{
	/*
	 * This class synthesizes the computer speech.
	 */
	public class Speaker
	{
		private Core core;
		private SpeechSynthesizer synth;

		public bool Init(Core core)
		{
			this.core = core;
			string lang = core.ActiveLanguage;
			synth = new SpeechSynthesizer ();
			try {
				synth.SetOutputToDefaultAudioDevice ();

				// This should be more scalable
				if (lang == "en") {
					synth.SelectVoice ("Microsoft Server Speech Text to Speech Voice (en-GB, Hazel)");
				} else if (lang == "es") {
					synth.SelectVoice ("Microsoft Server Speech Text to Speech Voice (es-ES, Helena)");
				}
			} catch (Exception e) {
				if (e.Message.Contains ("voice was disabled")) {
					throw e;
				}
				return false;
			}

			return true;
		}

		public void ChangeLanguage(string lang)
		{
			// This should be more scalable too
			if (lang == "en") {
				synth.SelectVoice ("Microsoft Server Speech Text to Speech Voice (en-GB, Hazel)");
			} else if (lang == "es") {
				synth.SelectVoice ("Microsoft Server Speech Text to Speech Voice (es-ES, Helena)");
			}
		}

		public void Speak(string tag, params string[] args)
		{
			synth.Speak (ReplaceArgs(core.Language.GetString(tag),args));
		}

		private String ReplaceArgs(string sentence, string[] args)
		{
			string replacedSentence = sentence;
			foreach (string arg in args)
			{
				Regex regex = new Regex(Regex.Escape("?"));
				replacedSentence = regex.Replace(replacedSentence, arg, 1); 
			}

			return replacedSentence;
		}
	}

}

