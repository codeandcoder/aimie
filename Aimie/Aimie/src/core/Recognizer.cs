using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.Speech.Recognition;

namespace Aimie
{
	/*
	 * The job of this class is to listen and recognize users speech.
	 */
	public class Recognizer
	{
		private Core core;
		private SpeechRecognitionEngine sre;
		private Dictionary<string, GrammarFile> grammars;
		private Thread recognitionThread;

		public bool Init(Core core, Dictionary<string, Module> modules)
		{
			this.core = core;
			grammars = GetAllGrammars (modules);
			List<string> allGrammars = RetrieveAllGrammarPhrases ();

			bool microOn = InitRecognitionEngine (allGrammars);
			if (microOn) {
				recognitionThread = new Thread (new ThreadStart (this.Recognizing));
				recognitionThread.Start ();
			}
			return microOn;
		}

		private bool InitRecognitionEngine(List<string> allGrammars)
		{
			string completeLanguage = "";

			if ("es".Equals (core.ActiveLanguage)) {
				completeLanguage = "es-ES";
			} else if ("en".Equals (core.ActiveLanguage)) {
				completeLanguage = "en-GB";
			}
			try {
				sre = new SpeechRecognitionEngine(new System.Globalization.CultureInfo(completeLanguage));
				sre.SetInputToDefaultAudioDevice ();

				Choices choices = new Choices ();
				choices.Add (allGrammars.ToArray ());

				GrammarBuilder gb = new GrammarBuilder ();
				gb.Culture = new System.Globalization.CultureInfo (completeLanguage);
				gb.Append (choices);

				Grammar g = new Grammar (gb);
				sre.LoadGrammar (g);

				sre.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs> (sre_SpeechRecognized);
			} catch (InvalidOperationException ex) {
				return false;
			}

			return true;
		}

		private void Recognizing()
		{
			while (true)
			{
				sre.Recognize();
			}
		}

		private void sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
		{
			FindModuleWithPhrase (e.Result.Text);
		}

		private void FindModuleWithPhrase(string text)
		{
			foreach (string key in grammars.Keys)
			{
				GrammarFile grammar = grammars[key];
				List<string> result = grammar.GetWholeGrammar(core.ActiveLanguage);
				if (result.Contains(text)) {
					Sentence sentence = grammar.GetSentenceWithText (text, core.ActiveLanguage);
					if (core.ResponseQueue != null) {
						core.ResponseQueue.Execute (sentence.ID, sentence.GetParameters (text), core.ResponseCaseCode);
						core.ClearResponseQueue();
					} else if (!core.SilenceMode || sentence.ID.Contains ("silence-off")) {
						core.Modules [key].Execute (sentence.ID, sentence.GetParameters(text), 0);
					}
					break;
				}
			}
		}

		public void ChangeLanguage(string lang)
		{
			List<string> allGrammars = RetrieveAllGrammarPhrases ();
			InitRecognitionEngine (allGrammars);
		}

		private Dictionary<string, GrammarFile> GetAllGrammars(Dictionary<string, Module> modules)
		{
			Dictionary<string, GrammarFile> grammars = new Dictionary<string, GrammarFile> ();
			foreach (string key in modules.Keys)
			{
				Module m = modules[key];
				GrammarFile file = m.RetrieveGrammarFile ();
				if (file != null)
				{
					grammars.Add (key, file);
				}
			}

			return grammars;
		}

		private List<string> RetrieveAllGrammarPhrases()
		{
			List<string> result = new List<string> ();
			foreach (string key in grammars.Keys)
			{
				GrammarFile grammar = grammars[key];
				result.AddRange(grammar.GetWholeGrammar(core.ActiveLanguage));
			}

			return result;
		}

		public void SaveGrammars(Dictionary<string, Module> modules)
		{
			foreach (string key in modules.Keys)
			{
				Module m = modules [key];
				string path = m.RetrieveGrammarFileName ();
				string folder = path.Substring(0,path.LastIndexOf('\\'));
				GramUtility.SaveGrammarFile (folder, grammars [key]);
			}
		}

	}
}

