﻿using System;
using System.Collections.Generic;

namespace Aimie
{
	/*
	 * This class represents the configuration file data.
	 */
	public class Configuration
	{
		private static string file;

		private static Dictionary<string, string> parameters;
		public static Dictionary<string, string> Parameters {
			get { return parameters; }
		}
		private static Dictionary<string, string> dirs;
		public static Dictionary<string, string> Directories {
			get { return dirs; }
		}
		private static Dictionary<string, string> files;
		public static Dictionary<string, string> Files {
			get { return files; }
		}
		private static Dictionary<string, string> webs;
		public static Dictionary<string, string> Webs {
			get { return webs; }
		}

		private Configuration(){
		}

		public static void Init(String f) {
			parameters = new Dictionary<string, string> ();
			dirs = new Dictionary<string, string> ();
			files = new Dictionary<string, string> ();
			webs = new Dictionary<string, string> ();
			file = f;
			string[] lines = System.IO.File.ReadAllLines(@f);
			foreach (string line in lines)
			{
				if (line.Length >= 2 && !"//".Equals(line.Trim().Substring(0,2)) )
				{
					string[] parts = line.Split (new char[] { '=' }, 2);
					string key = parts [0].Trim ();
					if ("file".Equals (key)) {
						string[] fileParts = parts [1].Split (new char[] { '=' }, 2);
						files.Add (fileParts[0].Trim(), fileParts[1].Trim());
					} else if ("dir".Equals (key)) {
						string[] fileParts = parts [1].Split (new char[] { '=' }, 2);
						dirs.Add (fileParts[0].Trim(), fileParts[1].Trim());
					} else if ("web".Equals (key)) {
						string[] fileParts = parts [1].Split (new char[] { '=' }, 2);
						webs.Add (fileParts[0].Trim(), fileParts[1].Trim());
					} else {
						parameters.Add (parts [0].Trim (), parts [1].Trim ());
					}
				}
			}
		}

		public static void SaveConfiguration() {
			List<string> result = new List<string> ();

			foreach (string key in parameters.Keys)
			{
				string value = parameters[key];
				result.Add (key + " = " + value);
			}
			foreach (string key in files.Keys)
			{
				string value = files[key];
				result.Add ("file = " + key + "=" + value);
			}
			foreach (string key in dirs.Keys)
			{
				string value = dirs[key];
				result.Add ("dir = " + key + "=" + value);
			}
			foreach (string key in webs.Keys)
			{
				string value = webs[key];
				result.Add ("web = " + key + "=" + value);
			}

			System.IO.File.WriteAllLines(@file, result.ToArray());
		}

		public static string GetParam(string key) {
			if (!parameters.ContainsKey (key)) {
				return null;
			}

			return parameters[key];
		}

		public static string GetDir(string key) {
			if (!dirs.ContainsKey (key)) {
				return null;
			}

			return dirs[key];
		}

		public static string GetFile(string key) {
			if (!files.ContainsKey (key)) {
				return null;
			}

			return files[key];
		}

		public static string GetWeb(string key) {
			if (!webs.ContainsKey (key)) {
				return null;
			}

			return webs[key];
		}

		public static void SetParam(string key, string value) {
			parameters [key] = value;
			SaveConfiguration ();
		}

		public static void SetDir(string key, string value) {
			dirs [key] = value;
			SaveConfiguration ();
		}

		public static void SetFile(string key, string value) {
			files [key] = value;
			SaveConfiguration ();
		}

	}
}

