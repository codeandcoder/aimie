using System;
using System.Collections.Generic;

namespace Aimie
{
	/*
	 * This class Looks for all modules in code and initialize whole system.
	 */
	public class Core
	{
		public static string[] LANGUAGES
		{
			get { return RetreiveSupportedLanguages (); }
		}

		public string ActiveLanguage
		{
			get { return Configuration.GetParam("active-lang"); }
			set { Configuration.SetParam("active-lang",value);
				speaker.ChangeLanguage(value);
				recognizer.ChangeLanguage(value);
			}
		}

		private bool silenceMode = false;
		public bool SilenceMode
		{
			get { return silenceMode; }
			set { silenceMode = value; }
		}

		private Dictionary<string, Module> modules;
		public Dictionary<string, Module> Modules
		{
			get { return modules; }
		}

		private Lang language = new Lang();
		public Lang Language
		{
			get { return language; }
		}

		private Speaker speaker = new Speaker ();
		public Speaker Speaker
		{
			get { return speaker; }
		}

		private Recognizer recognizer = new Recognizer();

		private Module responseQueue;
		public Module ResponseQueue
		{
			get { return responseQueue; }
		}
		private int responseCaseCode;
		public int ResponseCaseCode
		{
			get { return responseCaseCode; }
		}
		public void SetResponseQueue(Module module, int caseCode)
		{
			responseQueue = module;
			responseCaseCode = caseCode;
		}
		public void ClearResponseQueue()
		{
			responseQueue = null;
		}

		public Core ()
		{
			Init ();
		}

		private void Init()
		{
			try {
				Configuration.Init ("config\\aimie.conf");
				modules = ModuleFinder.FindModules (this);
				language.Init (this, modules);
				if (speaker.Init (this)) {
					speaker.Speak ("init");
					if (recognizer.Init (this, modules)) {
						speaker.Speak ("initialized", language.GetString ("aimie"));
						SaveAll ();
					} else {
						speaker.Speak ("no-micro");
					}
				} else {
					// Show window?
				}
			} catch(Exception ex) {
				System.Diagnostics.Process.Start ("https://bitbucket.org/awidev/aimie/wiki/Dependencies%20Problem");
			}
		}

		public void SaveAll()
		{
			Configuration.SaveConfiguration ();
			recognizer.SaveGrammars (modules);
			language.SaveStrings ();
		}

		private static string[] RetreiveSupportedLanguages()
		{
			string[] langs = Configuration.GetParam("supported-langs").Split ('|');
			List<string> formattedLangs = new List<string> ();

			foreach (string lang in langs)
			{
				formattedLangs.Add (lang.Trim ());
			}

			return formattedLangs.ToArray ();
		}

	}
}

