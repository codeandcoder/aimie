using System;
using System.Collections.Generic;

namespace Aimie
{
	/*
	 * Language related functionality
	 */
	public class Lang
	{
		private Core core;
		private Dictionary<string,Dictionary<string,Dictionary<string,string>>> strings;

		public void Init(Core core, Dictionary<string, Module> modules)
		{
			this.core = core;
			strings = new Dictionary<string,Dictionary<string,Dictionary<string,string>>> ();
			foreach (string key in modules.Keys)
			{
				Module m = modules [key];
				string file = m.RetrieveStringsFileName ();
				Dictionary<string,Dictionary<string,string>> fileStrings = m.RetrieveStringsFile();
				if ( fileStrings != null )
				{ 
					strings.Add (file, fileStrings);
				}
			}
		}

		public string GetString(string tag)
		{
			foreach (string file in strings.Keys)
			{
				Dictionary<string,Dictionary<string,string>> fileStrings = strings [file];
				try {
					return fileStrings [core.ActiveLanguage] [tag];
				}
				catch (KeyNotFoundException e){ /* Do nothing */ }
			}

			return null;
		}

		public void SaveStrings()
		{
			StringsUtility.WriteStrings (strings);
		}

	}
}

