using System;
using System.Collections.Generic;
using System.Reflection;

namespace Aimie
{
	/*
	 * This class looks for all modules in code and instantiates them.
	 */
	public class ModuleFinder
	{
		private ModuleFinder ()
		{
		}

		public static Dictionary<string,Module> FindModules(Core core)
		{
			Dictionary<string,Module> modules = new Dictionary<string,Module>();

			Assembly myAssembly = Assembly.GetExecutingAssembly();
			foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
			{
				foreach (Type type in a.GetTypes())
				{
					if (type.IsSubclassOf (typeof(Module)))
					{
						Module module = (Module)Activator.CreateInstance (type);
						modules.Add (type.Name, module);
						// Inits module.
						module.Init (core);
					}
				}
			}

			return modules;
		}
	}
}

